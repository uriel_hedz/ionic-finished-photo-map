import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Transaction} from './../../database';
import {GeolocationService} from '../../services/geolocation.service';
import { Camera } from 'ionic-native';
import {TransactionService} from '../../services/transaction.service';

/*
  Generated class for the Adding page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-adding',
  templateUrl: 'adding.html'
})

export class AddingPage {

  title: String = "Agregar transacción 🆕";
  shouldGeolocate: boolean;
  shouldSend: boolean = true;
	model: Transaction;
  imageData: String ;
  positive : boolean = false;
  
  constructor(public navCtrl: NavController,
              public geolocator: GeolocationService,
              private transactionService: TransactionService) {
  	this.model = this.cleanTransaction();  	
  }

  getPhoto(){
    let options = {
      quality: 10,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      targetWidth: 100,
      targetHeight:100
    }

    Camera.getPicture(options).then((imageData) => {
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     console.log(base64Image);
     this.imageData = base64Image;

     this.model.imageUrl = imageData;


    }, (err) => {
     // Handle error
     console.log(err)
    });
  }

  getLocation(){
  	if(!this.shouldGeolocate){
  		console.log("Cleaning coords");
  		this.model.cleanCoords();
  	}else if(this.model.emptyCoords()){
  		this.shouldSend = false;
  		this.geolocator.get().then(result =>{
	  		this.shouldSend = true;
	    	this.model.setCoords(result.coords);
	    }).catch(err =>{
        console.log(err);
     });
  	}
  }
  convertInOut(){
    let amount = parseInt(this.model.amount+"");
    if(this.positive) return amount;
    return amount * -1;
      
  }
  save(){
      this.model.amount = this.convertInOut();

    	// this.model.save().then(result =>{
	    //   console.log(result);
	    // });
      this.transactionService.save(this.model);

	    this.model = this.cleanTransaction();

	    this.navCtrl.pop();	
    
  }

  cleanTransaction(){
    return new Transaction(null,"");
  }

  


}


	
