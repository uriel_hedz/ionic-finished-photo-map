import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import {Transaction,IWallet} from './../../database';

import {AddingPage} from '../adding/adding';

import {WalletLSService} from '../../services/wallet.service';

@Component({
  selector: 'page-home',
  templateUrl: 'transactions.html'
})
export class TransactionsPage {
	title: String = "Movimientos";
	transactions:any;
  addingPage = AddingPage;
  wallet : IWallet = {amount:0,name:""};


  constructor(public navCtrl: NavController, public walletLS : WalletLSService) {
     // indexedDB.deleteDatabase("TransactionAppDB");
     // localStorage.clear();
  }

  ionViewWillEnter(){
    if(this.walletLS.empty()){
      this.walletLS.validateFirstWallet().then( ()=> this.loadTransactions() );  
    }else{
      this.loadTransactions()
      this.loadWallet()
    }    
    
  }
  loadWallet(){
    this.walletLS.get().then(wallet => this.wallet = wallet);
  }
  loadTransactions(){
    Transaction.all()
              .then(results => this.transactions = results);
  }
  

}
