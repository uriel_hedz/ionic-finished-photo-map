import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Wallet,IWallet} from '../../database';
import {WalletLSService} from '../../services/wallet.service';
import { NewWalletPage} from '../new-wallet/new-wallet';
import {Toast} from 'ionic-native';

@Component({
  selector: 'page-wallets-page',
  templateUrl: 'wallets-page.html'
})
export class WalletsPage {
	wallets : IWallet[];
  addingPage = NewWalletPage;

  constructor(public navCtrl: NavController, public walletLS : WalletLSService) {}

  set(wallet){
    this.walletLS.setID(wallet.id);
  }

  delete(wallet){
    if(this.wallets.length == 1){
      return this.showToast("Debes conservar al menos 1 cartera","top");
    }
    this.wallets = this.wallets.filter(w =>{
      console.log(w)
      return w.id != wallet.id;
    });
    // if(this.walletLS.getID())
    wallet.destroy();    
  }

  ionViewWillEnter() {
    Wallet.all().then((result)=> this.wallets = result);
  }

  showToast(message, position) {
    Toast.show(message, "short", position).subscribe(
        toast => {
            console.log(toast);
        }
    );
  }

}
