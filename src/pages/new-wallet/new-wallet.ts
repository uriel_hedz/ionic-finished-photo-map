import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Wallet } from '../../database';
import { WalletLSService } from '../../services/wallet.service';

@Component({
  selector: 'page-new-wallet',
  templateUrl: 'new-wallet.html'
})
export class NewWalletPage {
	model : Wallet = new Wallet(0,"");
  constructor(public navCtrl: NavController, public walletLS : WalletLSService) {}

  ionViewDidLoad() {
  }

  save(){
  	this.model.amount = +this.model.amount;
  	this.model.save().then(() =>{
  		this.walletLS.setID(this.model.id);

  		this.navCtrl.pop();	
  	});

  }

}
