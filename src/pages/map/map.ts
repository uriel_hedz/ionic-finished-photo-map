import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GoogleMap, GoogleMapsEvent,GoogleMapsLatLng,CameraPosition,GoogleMapsMarkerOptions,GoogleMapsMarker } from 'ionic-native';
import {GeolocationService} from '../../services/geolocation.service';
import {Transaction} from './../../database';
/*
  Generated class for the Map page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})


export class MapPage {
  map : GoogleMap = null;

  constructor(public geolocator: GeolocationService,public navCtrl: NavController) {
    
  }


  loadMap(coords){
    let location: GoogleMapsLatLng = new GoogleMapsLatLng(coords.latitude,coords.longitude);
    // alert(coords.latitude);
    // alert(coords.longitude);
    this.map = new GoogleMap('map', {
      'backgroundColor': 'white',
      'controls': {
        'compass': true,
        'myLocationButton': true,
        'indoorPicker': true,
        'zoom': true
      },
      'gestures': {
        'scroll': true,
        'tilt': true,
        'rotate': true,
        'zoom': true
      },
      'camera': {
        'latLng': location,
        'tilt': 30,
        'zoom': 15,
        'bearing': 50
      }
    });

    this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => this.loadMarkers())
    
    // 

    // // create CameraPosition
    // let position: CameraPosition = {
    //   target: ionic,
    //   zoom: 18,
    //   tilt: 30
    // };

    // // move the map's camera to position
    // map.setCenter(ionic);

  }
  loadMarkers(){
    Transaction.all()
              .then((results) => this.loadTransactionMarkers(results));
  }

  loadTransactionMarkers(transactions){
    for (var i = transactions.length - 1; i >= 0; i--) {
      let transaction : Transaction = transactions[i];
      let markerLocation: GoogleMapsLatLng = new GoogleMapsLatLng(
                                                  Number(transaction.lat),
                                                  Number(transaction.lng)
                                             );
      console.log(markerLocation);

      let markerOptions: GoogleMapsMarkerOptions = {
        position: markerLocation,
        title: transaction.title,
        icon: 'data:image/jpeg;base64,'+transaction.imageUrl
      };

      this.map.addMarker(markerOptions)
        .then((marker: GoogleMapsMarker) => {
          marker.showInfoWindow();
        }).catch(err => console.log(err));


    }
  }

  ionViewDidLoad() {
    this.geolocator.get().then(()=>this.loadMap({latitude: 43.0741904,longitude:-89.3809802}) );
    ;
  }

}
