import Dexie from '../node_modules/dexie/dist/dexie';
import {WalletLSService} from './services/wallet.service';
export class TransactionAppDB extends Dexie{

	transactions: Dexie.Table<ITransaction, number>;
    categories: Dexie.Table<ICategory, number>;
    photos: Dexie.Table<IPhoto, number>;
    wallets: Dexie.Table<IWallet, number>;

	constructor(){
		super("TransactionAppDB");

        this.version(1).stores({
          transactions: '++id, amount, lat,lng,categoryId,walletId',
          categories: '++id, title, color',
          wallets: '++id,amount,name'
        });

        this.transactions.mapToClass(Transaction);
    	this.wallets.mapToClass(Wallet);
	};
}

export interface ICategory {
    id?: number;
    title: string; 
    color: string;
}

export interface IPhoto {
    id?: number;
    url: string;
}

export interface ITransaction {
    id?: number; // Primary key. Optional (autoincremented)
    amount: number;
    lat: string; // Last name
    lng: string;
    title: string;
    imageUrl: string;
    categoryId: number; // "Foreign key" to an IContact
    walletId: number; // "Foreign key" to an IContact
}

export interface IWallet {
    id?: number; // Primary key. Optional (autoincremented)
    amount: number;
    name: string;
}

// In same file due to lack of control on how files are imported
export class Transaction {
    // ++id, amount, lat,lng,categoryId
    id: number;
    amount: number;
    lat: string;
    lng: string;
    categoryId: number;
    title:string;
    imageUrl : string;
    walletId : number;
    createdAt: Date;

    constructor(amount: number,title:string, lat?: string, lng?: string,
                categoryId?: number, id?: number,walletId?: number, imageUrl?: string) {
        this.amount = amount;

        if (lat) this.lat = lat;
        if (lng) this.lng = lng;
        if (imageUrl) this.imageUrl = imageUrl;
        if (categoryId) this.categoryId = categoryId;
        if (id) this.id = id;
    }

    save() {
        // Returns promise
      this.walletId = parseInt(WalletLSService.getID());
      if(!this.id) this.createdAt = new Date();

      return db.transactions.add(this);
    }

    setCoords(coords){

        this.lat = 43.075836+"";
        this.lng = -89.380755+"";
    }

    cleanCoords(){
      this.lat = "";
      this.lng = "";
    }


    formattedLat(){
      if(!this.lat || this.lat == "")
        return 0 ;
      return this.toNumber(this.lat);
    }

    formattedLng(){
      if(!this.lng || this.lng == "")
        return 0 ;
      return this.toNumber(this.lng);
    }

    toNumber(str){
      let commaPos = str.indexOf(',');
      return parseFloat(str.substring(0, commaPos));
    }

    emptyCoords(){
      if(!this.lat || !this.lng) return true;
      return this.lat.length <= 0 && this.lng.length <= 0;
    }

    static all(){
        // Returns promise;
        return db.transactions
              .where("walletId")
              .equals(parseInt(WalletLSService.getID()))
              .reverse()
              .toArray();
    }

    static create(args) {
        new Transaction(args.amount, args.lat, args.lng, args.categoryId).save();
    }

}

export class Wallet implements IWallet{
  amount : number;
  name : string;
  id : number;
  constructor(amount: number,name:string, id?:number) {
    this.amount = amount;
    this.name = name;
    if(id){this.id = id}
  }

  save() {

    return db.wallets.add(this);
  }

  destroy(){
    db.wallets.delete(this.id);
  }

  update(transaction: Transaction){
    this.amount += transaction.amount;
    this.save();
  }

  static update(id,amount: number){
    return db.wallets.update(id,{amount: amount})
  }

  static find(id: number){
    return db.wallets.get(id)
  }

  static first(){
    return db.wallets.orderBy("id").limit(1).first();
  }

  static all(){
      // Returns promise;
    return db.wallets
          .orderBy("id")
          .reverse()
          .toArray();
  }

  static count(){
      // Returns promise;
    return db.wallets.count();
  }

  static createFirst(){
      // Returns promise;
      // alert("Creating");
    let wallet = new Wallet(0,"Mi primera cartera")
    return wallet.save();
  }

}


export let db = new TransactionAppDB();