import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { TabsPage } from '../pages/tabs/tabs';
import db from '../database';
import {GeolocationService} from '../services/geolocation.service'
import { TokensService } from '../services/tokens.service';

declare var FCMPlugin;
@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`,
  providers: [GeolocationService]
})
export class MyApp {
  rootPage = TabsPage;


  constructor(platform: Platform,private tokensService: TokensService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      FCMPlugin.getToken(
        function (token) {
          console.log("Here comes the token")
          tokensService.send(token)
                       .subscribe((r)=>{
                         console.log("Enviamos el token ;)")
                         console.log(r);
                       },(err)=>{
                         console.log("No pudimos enviar el token");
                         console.log(err);
                       })
        },
        function (err) {
          alert(err);
          console.log('error retrieving token: ' + err);
        }
      );

      StatusBar.styleDefault();
    });
  }
}
