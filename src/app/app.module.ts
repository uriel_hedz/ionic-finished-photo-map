import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { MapPage } from '../pages/map/map';
import { ContactPage } from '../pages/contact/contact';
import { TransactionsPage } from '../pages/transactions/transactions';
import { TabsPage } from '../pages/tabs/tabs';
import { AddingPage } from '../pages/adding/adding';
import { WalletsPage } from '../pages/wallets-page/wallets-page';
import {WalletLSService} from '../services/wallet.service';
import {TransactionService} from '../services/transaction.service';
import {TokensService} from '../services/tokens.service';
import { NewWalletPage } from '../pages/new-wallet/new-wallet';
import { GeolocationService } from '../services/geolocation.service';

@NgModule({
  declarations: [
    MyApp,
    MapPage,
    ContactPage,
    TransactionsPage,
    TabsPage,
    AddingPage,
    WalletsPage,
    NewWalletPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    ContactPage,
    TransactionsPage,
    TabsPage,
    AddingPage,
    WalletsPage,
    NewWalletPage
  ],
  providers: [WalletLSService,TransactionService,TokensService,GeolocationService]
})
export class AppModule {}
