import {Injectable} from "@angular/core";
import {Wallet,IWallet} from "../database";

@Injectable()
export class WalletLSService{
	setID(walletID){
		localStorage.setItem("walletID",walletID);
	}

	getID() : string{
		return localStorage.getItem("walletID");
	}

	getIDNumeric() : number{
		return parseInt(localStorage.getItem("walletID"));
	}

	setToFirst(){
		Wallet.first().then((w) => {
			new WalletLSService().setID(w.id)
		})
	}

	empty():boolean{
		return !localStorage.getItem("walletID");
	}

	get() : any{
		return Wallet.find(this.getIDNumeric());
	}

	update(amount: number){
		let findPromise = Wallet.find(this.getIDNumeric());
		let updatePromise = findPromise.then(wallet =>{
			Wallet.update(this.getIDNumeric(),(+wallet.amount) + (+amount));
		});

		return Promise.all([findPromise,updatePromise]);		
	}

	static getID(){
   return localStorage.getItem("walletID");	 
  }

  checkInit(){
  	if(!localStorage.getItem("walletID")){
  		this.setToFirst();
  	}
  }

  validateFirstWallet(){
	    return new Promise((resolve,reject)=>{
	    	 // alert("Valideitink");
	    	 Wallet.first().then((w)=>{
		        if(!w){
		          Wallet.createFirst().then((result) => {
		          	this.setID(result);
		          	resolve();
		          });
		      	}else{
		      		this.setID(w.id);
		      		resolve();
		      	}
			    }); 
	    });
	          
    }    
}