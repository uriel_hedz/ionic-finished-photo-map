// let transactionSaved = db.transactions.add(this);
//       let walletUpdated = this.walletLS.update(-20);;
import {Injectable} from "@angular/core";
import {Wallet,IWallet,Transaction} from "../database";
import {WalletLSService} from './wallet.service';

@Injectable()
export class TransactionService{
	constructor(private walletService : WalletLSService){}
	save(transaction : Transaction){
		console.log(":()");
		let transactionSavedPromise = transaction.save();
		let walletUpdatedPromise = this.walletService.update(transaction.amount);
		return Promise.all([transactionSavedPromise,walletUpdatedPromise]);
	}
}