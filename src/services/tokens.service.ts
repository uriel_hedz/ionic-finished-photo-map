// let transactionSaved = db.transactions.add(this);
//       let walletUpdated = this.walletLS.update(-20);;
import {Injectable} from "@angular/core";
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import {Observable} from 'rxjs'

const endpoint = "http://4b988702.ngrok.io"

@Injectable()
export class TokensService{
	constructor(private http : Http){}
	send(token:any){
		let data = {
							token: token,
							os: "android"
						};
		
		return this.http.post(endpoint+"/devices",data,this.options())
							 .map((r : Response)=> r.text())
							 .catch((err)=> {
							 	console.log("Ubo un error")
							 	console.log(err.text());
							 	return Observable.throw(err.json().error || "Error");
							 })

	}
	options() : RequestOptions{
		let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    return new RequestOptions({ headers: headers });
	}
}